﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(MeshCollider))]
[RequireComponent(typeof(MeshFilter))]
public partial class ClothPhysics : MonoBehaviour
{
	private IVerletIntegrator VerletIntegrator;

	private float fixedDT;
	private Vector3 gravity;
	private Vector3 wind;
	private readonly Vector4 gScaledV4Down = Vector3.down * 0.9f;

	private ConstraintCollection constraintCollection;
	private Constraint[] constraints;

	private Vector3[][] verticesCache;
	private Vector3[] normalsCache;

	private int currentCacheIndex = 0;

	private Vector3[] acceleration;
	private bool[] pinned;

    private void GenerateMesh ()
    {
		gameObject.CheckAndInitializeWithInterface(ref targetMeshFilter);
		gameObject.CheckAndInitializeWithInterface(ref targetMeshCollider);

        float accumulatedX = HorizontalTiling * HorizontalSpacing * -0.5f;
        float accumulatedY = VerticalTiling * VerticalSpacing * -0.5f;
		
		int[][] triangleIndices = new int[2][] { new int[] { 0, HorizontalTiling, 1 }, new int[] { 1, HorizontalTiling, HorizontalTiling + 1 } };

		verticesCache = new Vector3[2][] { new Vector3[HorizontalTiling * VerticalTiling], new Vector3[HorizontalTiling * VerticalTiling] };
		normalsCache = new Vector3[HorizontalTiling * VerticalTiling];

		acceleration = new Vector3[HorizontalTiling * VerticalTiling];
		penetrationDistances = new float[HorizontalTiling * VerticalTiling];
		pinned = new bool[HorizontalTiling * VerticalTiling];

		////for (int i = 0; i < penetrationDistances.Length; i ++)
		////{
		////	penetrationDistances[i] = 1f;
		////}

		pinned[0] = true;

		////pinned[HorizontalTiling / 2] = true;
		////if (HorizontalTiling % 2 == 0)
		////{
		////	pinned[HorizontalTiling / 2 - 1] = true;
		////}

		pinned[HorizontalTiling - 1] = true;

		Vector2[] uvs = new Vector2[HorizontalTiling * VerticalTiling];

		List<int> triangles = new List<int>();

		for (int y = 0; y < VerticalTiling; y++)
		{
			accumulatedX = HorizontalTiling * HorizontalSpacing * -0.5f;

			for (int x = 0; x < HorizontalTiling; x++)
            {
				var index = y * HorizontalTiling + x;

				verticesCache[0][index].x = accumulatedX;
				verticesCache[0][index].y = accumulatedY;
				
				verticesCache[1][index].x = accumulatedX;
				verticesCache[1][index].y = accumulatedY;
				
				normalsCache[index] = -Vector3.forward;
				uvs[index].x = (float)x / (float)HorizontalTiling;
				uvs[index].y = (float)y / (float)VerticalTiling;

				if ((y < VerticalTiling - 1) && (x < HorizontalTiling - 1))
				{
					var arr = triangleIndices[0];

					triangles.Add(index + arr[0]);
					triangles.Add(index + arr[1]);
					triangles.Add(index + arr[2]);

					arr = triangleIndices[1];

					triangles.Add(index + arr[0]);
					triangles.Add(index + arr[1]);
					triangles.Add(index + arr[2]);
				}

				accumulatedX += HorizontalSpacing;
            }

			accumulatedY += VerticalSpacing;
        }

		var mesh = new Mesh();

		targetMeshFilter.mesh = mesh;

		mesh.vertices = verticesCache[0];
		mesh.uv = uvs;
		mesh.normals = normalsCache;
		mesh.SetTriangles(triangles, 0);
    }

	void GenerateConstraints(ref Vector3[] vertices)
	{
		// Connecting immediate neighbor particles with constraints (distance 1 and sqrt(2) in the grid)
		var constraintsList = new List<Constraint>();

		Constraint constraint;

		for (int x = 0; x < HorizontalTiling; x++)
		{
			for (int y = 0; y < VerticalTiling; y++)
			{
				if (x < HorizontalTiling - 1)
				{
					constraint = new Constraint(y * HorizontalTiling + x, y * HorizontalTiling + x + 1, ref vertices, Constraint.EConstraintType.Stretch);
					constraintsList.Add(constraint);
				}

				if (y < VerticalTiling - 1)
				{
					constraint = new Constraint(y * HorizontalTiling + x, (y + 1) * HorizontalTiling + x, ref vertices, Constraint.EConstraintType.Stretch);
					constraintsList.Add(constraint);
				}

				if (x < HorizontalTiling - 1 && y < VerticalTiling - 1)
				{
					constraint = new Constraint(y * HorizontalTiling + x, (y + 1) * HorizontalTiling + x + 1, ref vertices, Constraint.EConstraintType.Shear);
					constraintsList.Add(constraint);
				}

				if (x < HorizontalTiling - 1 && y < VerticalTiling - 1)
				{
					constraint = new Constraint(y * HorizontalTiling + x + 1, (y + 1) * HorizontalTiling + x, ref vertices, Constraint.EConstraintType.Shear);
					constraintsList.Add(constraint);
				}
			}
		}


		// Connecting secondary neighbors with constraints (distance 2 and sqrt(4) in the grid)
		for (int x = 0; x < HorizontalTiling; x++)
		{
			for (int y = 0; y < VerticalTiling; y++)
			{
				if (x < HorizontalTiling - 2)
				{
					constraint = new Constraint(y * HorizontalTiling + x, y * HorizontalTiling + x + 2, ref vertices, Constraint.EConstraintType.Bend);
					constraintsList.Add(constraint);
				}

				if (y < VerticalTiling - 2)
				{
					constraint = new Constraint(y * HorizontalTiling + x, (y + 2) * HorizontalTiling + x, ref vertices, Constraint.EConstraintType.Bend);
					constraintsList.Add(constraint);
				}
			}
		}

		constraints = constraintsList.ToArray();

		constraintCollection = new ConstraintCollection(constraintsList);
	}

	Vector3 PointOnEllipse(float t, float y, float rx, float rz)
	{
		Vector3 v = new Vector3();
		v.y = y;

		Mathf.Clamp01(t);

		v.x = rx * Mathf.Cos(t * 2 * Mathf.PI);
		v.z = rz * Mathf.Sin(t * 2 * Mathf.PI);

		return v;
	}

	void AddForce(int index, Vector3 force)
	{
		acceleration[index] += force / particleMass;
	}

	void AddUniformForce(Vector3 force)
	{
		for (int i = 0; i < acceleration.Length; i ++)
		{
			AddForce(i, force);
		}
	}

	void AddNormalForce(int index, Vector3 force)
	{
		Vector3 normal = normalsCache[index];
		Vector3 f = normal * Vector3.Dot(normal, force);
		AddForce(index, f);
	}

	void AddUniformNormalForce(Vector3 force)
	{
		for (int i = 0; i < acceleration.Length; i++)
		{
			AddNormalForce(i, force);
		}
	}

	void FixedUpdate()
	{
		AddUniformNormalForce(wind);

		VerletIntegrator.Execute(currentCacheIndex, ref verticesCache, ref acceleration, pinned, damping, fixedDT, gravity);

		var oldVCache = verticesCache[currentCacheIndex];

		currentCacheIndex = (currentCacheIndex + 1) % 2;

		var currentVCache = verticesCache[currentCacheIndex];

		CheckCollisions(ref currentVCache, ref oldVCache);

		for (int i = 0; i < constraintIterations; i ++)
			constraintCollection.SatisfyConstraintsCPU(ref currentVCache, ref pinned, () => CheckCollisions(ref currentVCache, ref oldVCache));
	}

	private void CheckCollisions(ref Vector3[] currentVCache, ref Vector3[] oldVCache)
	{
		//check for collisions
		for (int j = 0; j < currentVCache.Length; j++)
		{
			RaycastHit hitInfo;

			var layerMask = LayerMask.GetMask("ClothCollider");

			var diff = oldVCache[j] - currentVCache[j];

			bool hit = Physics.Raycast(transform.localToWorldMatrix * oldVCache[j], -(transform.localToWorldMatrix * diff.normalized), out hitInfo, 2.5f, ~layerMask);

			penetrationDistances[j] -= 1e-2f;
			Mathf.Clamp01(penetrationDistances[j]);

			if (hit)
			{
				var mag = diff.magnitude;
				var invPenetrationScale = hitInfo.distance / mag;
				if (invPenetrationScale < 1f)
				{
					penetrationDistances[j] = 1f - invPenetrationScale;
					var rebound = Vector3.Reflect(diff, hitInfo.normal);

					//float outwards = Vector3.Dot(rebound, hitInfo.normal) < 0 ? -1f : 1f;

					currentVCache[j] = (Vector3)(transform.worldToLocalMatrix * (hitInfo.point + hitInfo.normal * 15e-3f)) + /*outwards */ rebound.normalized * Mathf.Clamp01(penetrationDistances[j]) * mag;
				}
			}
		}
	}

	private void InitIntegrator()
	{
		if (VerletIntegrator != null)
			VerletIntegrator.Dispose();

		VerletIntegrator = UseGPUIntegrator
			? (IVerletIntegrator)new GPUVerletIntegrator (verletIntegrationShader, HorizontalTiling, VerticalTiling)
			: (IVerletIntegrator)new CPUVerletIntegrator();
	}

    private void Awake ()
    {	
		GenerateMesh();
		GenerateConstraints(ref verticesCache[0]);
		
		//recalculate normals
		targetMeshFilter.mesh.RecalculateNormals();
		
		//update mesh collider
		targetMeshCollider.sharedMesh = targetMeshFilter.mesh;
	}

	private void Start()
	{
		InitIntegrator(); 
	}

	// Update is called once per frame
	private void Update ()
    {
		//assigned only once per frame instead of FixedUpdate

		fixedDT = Time.fixedDeltaTime;
		gravity = transform.worldToLocalMatrix * gScaledV4Down;

		wind = windDirection;

		wind.x = wind.x + Random.Range(-windDirectionVariance.x, windDirectionVariance.x);
		wind.y = wind.y + Random.Range(-windDirectionVariance.y, windDirectionVariance.y);
		wind.z = wind.z + Random.Range(-windDirectionVariance.z, windDirectionVariance.z);

		var intensity = windIntensity + Random.Range(-windIntensityVariance, windIntensityVariance);

		wind = wind.normalized * intensity;

		targetMeshFilter.mesh.vertices = verticesCache[currentCacheIndex];

		targetMeshFilter.mesh.normals = normalsCache;

		//recalculate normals
		targetMeshFilter.mesh.RecalculateNormals();

		//update mesh collider
		targetMeshCollider.sharedMesh = targetMeshFilter.mesh;

		normalsCache = targetMeshFilter.mesh.normals;

	}

	private void OnDestroy()
	{
		if (VerletIntegrator != null)
			VerletIntegrator.Dispose();
	}
}
