﻿using UnityEngine;
using System.Linq;

public class GPUVerletIntegrator : IVerletIntegrator
{
	public ComputeShader integratorShader;
	public ComputeBuffer vertexBuffer;
	public ComputeBuffer accelerationBuffer;
	public ComputeBuffer pinnedBuffer;

	public int HorizontalTiling
	{
		get { return horizontalTiling; }
		set
		{
			if (value != horizontalTiling)
			{
				horizontalTiling = value;

				AllocateBuffers();
			}
		}
	}

	private int horizontalTiling;

    public int VerticalTiling
    {
        get { return verticalTiling; }
        set
        {
            if (value != verticalTiling)
            {
                verticalTiling = value;

                AllocateBuffers();
            }
        }
    }

    private int verticalTiling;

    private void AllocateBuffers()
	{
		if (vertexBuffer != null)
			vertexBuffer.Release();

		if (accelerationBuffer != null)
			accelerationBuffer.Release();

		if (pinnedBuffer != null)
			pinnedBuffer.Release();

		vertexBuffer = new ComputeBuffer(HorizontalTiling * VerticalTiling * 2, sizeof(float) * 3);
		accelerationBuffer = new ComputeBuffer(HorizontalTiling * VerticalTiling, sizeof(float) * 3);
		pinnedBuffer = new ComputeBuffer(HorizontalTiling * VerticalTiling, sizeof(int));
	}

	public GPUVerletIntegrator(ComputeShader shader, int horizontalTiling, int verticalTiling)
	{
		this.integratorShader = shader;
		this.horizontalTiling = horizontalTiling;
        this.verticalTiling = verticalTiling;

		AllocateBuffers();
	}

	public void Execute(int currentCacheIndex, ref Vector3[][] verticesCache, ref Vector3[] acceleration, bool[] pinned, float damping, float fixedDT, Vector3 gravity)
	{
		vertexBuffer.SetData(verticesCache.SelectMany(x => x).ToArray());
		accelerationBuffer.SetData(acceleration);
		pinnedBuffer.SetData(pinned.Select(x => (x == true) ? 1 : 0).ToArray());

        int kernelIndex = integratorShader.FindKernel("VerletIntegrator");

		integratorShader.SetBuffer(kernelIndex, "vertexBuffer", vertexBuffer);
		integratorShader.SetBuffer(kernelIndex, "accelerationBuffer", accelerationBuffer);
		integratorShader.SetBuffer(kernelIndex, "pinnedBuffer", pinnedBuffer);
		integratorShader.SetFloat("damping", damping);
		integratorShader.SetFloat("fixedDT", fixedDT);
		integratorShader.SetVector("gravity", gravity);
		integratorShader.SetInt("currentCacheIndex", currentCacheIndex);
		integratorShader.SetInt("horizontalTiling", HorizontalTiling);
        integratorShader.SetInt("verticalTiling", VerticalTiling);

        uint xthreads, ythreads, zthreads;

        integratorShader.GetKernelThreadGroupSizes(kernelIndex, out xthreads, out ythreads, out zthreads);

        integratorShader.Dispatch(kernelIndex, Mathf.CeilToInt(HorizontalTiling / (float)xthreads), Mathf.CeilToInt(VerticalTiling / (float)ythreads), 1);

		Vector3[] tempData = new Vector3[HorizontalTiling * VerticalTiling * 2];

		vertexBuffer.GetData(tempData);
        accelerationBuffer.GetData(acceleration);

		for (int i = 0; i < verticesCache.Length; i ++)
		{
			for (int j = 0; j < HorizontalTiling * VerticalTiling; j ++)
			{
				verticesCache[i][j] = tempData[i * HorizontalTiling * VerticalTiling + j];
			}
		}
	}

	public void Dispose()
	{
		if (vertexBuffer != null)
			vertexBuffer.Release();

		if (accelerationBuffer != null)
			accelerationBuffer.Release();

		if (pinnedBuffer != null)
			pinnedBuffer.Release();
	}
}
