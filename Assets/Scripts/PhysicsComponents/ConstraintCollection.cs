﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ConstraintCollection
{
	private int[] v1indices;
	private int[] v2indices;

	private float[] restDistances;

	private static readonly Vector3 vZero = Vector3.zero;

	public ConstraintCollection(IEnumerable<Constraint> sourceConstraints)
	{
		var count = sourceConstraints.Count();
		v1indices = new int[count];
		v2indices = new int[count];

		restDistances = new float[count];

		int i = 0;

		foreach (var constraint in sourceConstraints)
		{
			v1indices[i] = constraint.v1index;
			v2indices[i] = constraint.v2index;

			restDistances[i] = constraint.restDistance;

			i++;
		}
	}

	public void SatisfyConstraintsCPU(ref Vector3[] vertices, ref bool[] pinned, Action CheckCollisions)
	{
		for (int i = 0; i < restDistances.Length; i++)
		{
			var v1index = v1indices[i];
			var v2index = v2indices[i];

			var restDistance = restDistances[i];

			Vector3 diffVector = vertices[v1index] - vertices[v2index];
			float currentDistance = diffVector.magnitude;
			Vector3 correctionVector = diffVector * (currentDistance - restDistance);
			Vector3 correctionVectorHalf = correctionVector * 0.5f;

			vertices[v1index] = vertices[v1index] - (pinned[v1index] ? vZero : (pinned[v2index] ? correctionVector : correctionVectorHalf));
			vertices[v2index] = vertices[v2index] + (pinned[v2index] ? vZero : (pinned[v1index] ? correctionVector : correctionVectorHalf));
		}

		for (int i = restDistances.Length - 1; i >= 0; i--)
		{
			var v1index = v1indices[i];
			var v2index = v2indices[i];

			var restDistance = restDistances[i];

			Vector3 diffVector = vertices[v1index] - vertices[v2index];
			float currentDistance = diffVector.magnitude;
			Vector3 correctionVector = diffVector * (currentDistance - restDistance);
			Vector3 correctionVectorHalf = correctionVector * 0.5f;

			vertices[v1index] = vertices[v1index] - (pinned[v1index] ? vZero : (pinned[v2index] ? correctionVector : correctionVectorHalf));
			vertices[v2index] = vertices[v2index] + (pinned[v2index] ? vZero : (pinned[v1index] ? correctionVector : correctionVectorHalf));
		}

		CheckCollisions();
	}

	public void SatisfyConstraintsGPU(ref Vector3[] vertices, ref bool[] pinned, Action CheckCollisions)
	{
		//TODO:
		//Get kernel config
		//Set buffer data
		//Set up buffers
		//Dispatch kernel
		//Retrieve buffer data

		CheckCollisions();
	}
}

