﻿using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public partial class ClothPhysics : MonoBehaviour
{
	private static readonly Color normalsColor = new Color(1f, 0f, 1f, 0.5f);
	private static readonly Color stretchConstraintColor = new Color(1f, 0f, 0f, 0.5f);
	private static readonly Color shearConstraintColor = new Color(0f, 1f, 0f, 0.5f);
	private static readonly Color bendConstraintColor = new Color(0f, 0f, 1f, 0.5f);
	private static readonly Color collisionColor = new Color(0f, 1f, 1f, 0.5f);

	[SerializeField]
	public bool showStretchConstraints;

	[SerializeField]
	public bool showShearConstraints;

	[SerializeField]
	public bool showBendConstraints;

	[SerializeField]
	public bool showSurfaceNormals;

	[SerializeField]
	public bool showCollisions;

	private float[] penetrationDistances;

	void OnDrawGizmosSelected()
	{
		var oldColor = Gizmos.color;
		// Display the explosion radius when selected
		Gizmos.color = normalsColor;

		if (showSurfaceNormals && (normalsCache != null))
		{
			for (int i = 0; i < normalsCache.Length; i++)
			{
				Vector3 vpos = transform.localToWorldMatrix * verticesCache[currentCacheIndex][i];
				Gizmos.DrawLine(vpos, vpos + (Vector3)(transform.localToWorldMatrix * normalsCache[i] * 0.1f));
			}
		}

		if (constraints != null)
		{
			for (int i = 0; i < constraints.Length; i++)
			{
				var constraint = constraints[i];

				switch (constraint.type)
				{
					case Constraint.EConstraintType.Stretch:
						{
							if (!showStretchConstraints)
								continue;
							var col = stretchConstraintColor;
							var diff = verticesCache[currentCacheIndex][constraint.v1index] - verticesCache[currentCacheIndex][constraint.v2index];
							var distance = diff.magnitude;
							col.r = Mathf.Clamp01(distance / constraint.restDistance * 0.5f);
							Gizmos.color = col; break;
						}
					case Constraint.EConstraintType.Shear:
						{
							if (!showShearConstraints)
								continue;
							var col = shearConstraintColor;
							var diff = verticesCache[currentCacheIndex][constraint.v1index] - verticesCache[currentCacheIndex][constraint.v2index];
							var distance = diff.magnitude;
							col.g = Mathf.Clamp01(distance / constraint.restDistance * 0.5f);
							Gizmos.color = col; break;
						}
					case Constraint.EConstraintType.Bend:
						{
							if (!showBendConstraints)
								continue;
							var col = bendConstraintColor;
							var diff = verticesCache[currentCacheIndex][constraint.v1index] - verticesCache[currentCacheIndex][constraint.v2index];
							var distance = diff.magnitude;
							col.b = Mathf.Clamp01(distance / constraint.restDistance * 0.5f);
							Gizmos.color = col; break;
						}
				}


				Vector3 v1 = transform.localToWorldMatrix * (verticesCache[currentCacheIndex][constraint.v1index] + normalsCache[constraint.v1index] * 0.05f);
				Vector3 v2 = transform.localToWorldMatrix * (verticesCache[currentCacheIndex][constraint.v2index] + normalsCache[constraint.v2index] * 0.05f);

				Gizmos.DrawLine(v1, v2);
			}
		}

		if (showCollisions && (penetrationDistances != null) && (transform != null) && (verticesCache != null) && (verticesCache[currentCacheIndex] != null))
		{
			for (int j = 0; j < penetrationDistances.Length; j++)
			{
				if (penetrationDistances[j] > 0f)
				{
					var col = collisionColor;
					col.g = penetrationDistances[j];
					col.b = penetrationDistances[j];
					Gizmos.color = col;
					Gizmos.DrawSphere(transform.localToWorldMatrix * verticesCache[currentCacheIndex][j], Mathf.Min(HorizontalSpacing, VerticalSpacing) * 0.5f);
				}
			}
		}

		Gizmos.color = oldColor;
	}
}
