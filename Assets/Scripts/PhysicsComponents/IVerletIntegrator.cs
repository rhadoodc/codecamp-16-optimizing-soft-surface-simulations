﻿using System;
using UnityEngine;

interface IVerletIntegrator : IDisposable
{
	int HorizontalTiling
	{
		get; set;
	}

	void Execute(int currentCacheIndex, ref Vector3[][] verticesCache, ref Vector3[] acceleration, bool[] pinned, float damping, float fixedDT, Vector3 gravity);
}
