﻿using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public partial class ClothPhysics : MonoBehaviour
{
    public MeshFilter targetMeshFilter;
	public MeshCollider targetMeshCollider;

	public int HorizontalTiling
	{
		get { return horizontalTiling; }
		set
		{
			if (horizontalTiling != value)
			{
				horizontalTiling = value;
				GenerateMesh();
			}
		}
	}

	[SerializeField]
	private int horizontalTiling;

    public int VerticalTiling
	{
		get { return verticalTiling; }
		set
		{
			if (verticalTiling != value)
			{
				verticalTiling = value;
				GenerateMesh();
			}
		}
	}

	[SerializeField]
	private int verticalTiling;

    public float HorizontalSpacing
	{
		get { return horizontalSpacing; }
		set
		{
			if (horizontalSpacing != value)
			{
				horizontalSpacing = value;
				GenerateMesh();
			}
		}
	}

	[SerializeField]
	private float horizontalSpacing;

    public float VerticalSpacing
	{
		get { return verticalSpacing; }
		set
		{
			if (verticalSpacing != value)
			{
				verticalSpacing = value;
				GenerateMesh();
			}
		}
	}

	[SerializeField]
	private float verticalSpacing;

	public bool UseGPUIntegrator
	{
		get { return useGPUIntegrator; }
		set
		{
            if (value != useGPUIntegrator)
            {
                useGPUIntegrator = value;

                InitIntegrator();
            }
		}
	}

	[SerializeField]
	private bool useGPUIntegrator;

	public int constraintIterations = 15;

	public float damping = 0.1f;

	public float particleMass = 1f;

	public Vector3 windDirection = Vector3.zero;
	public Vector3 windDirectionVariance = Vector3.zero;

	public float windIntensity = 0;
	public float windIntensityVariance = 0;

	public ComputeShader verletIntegrationShader;
	public ComputeShader constraintShader;
}
