﻿using UnityEngine;

public partial class ClothPhysics
{
	float epsilon = 1e-6f;

	float IntersectTriangle(Ray ray, int v0index, int v1index, int v2index, out Vector3 intersection)
	{
		return IntersectTriangle(ray.origin, ray.direction, verticesCache[currentCacheIndex][v0index], verticesCache[currentCacheIndex][v1index], verticesCache[currentCacheIndex][v2index], out intersection);
	}


	float IntersectTriangle(Ray ray, Vector3 v0, Vector3 v1, Vector3 v2, out Vector3 intersection)
	{
		return IntersectTriangle(ray.origin, ray.direction, v0, v1, v2, out intersection);
	}

	float IntersectTriangle(Vector3 rayOrigin, Vector3 rayDirection, Vector3 v0, Vector3 v1, Vector3 v2, out Vector3 intersection)
	{
		intersection = Vector3.zero;

		float u, v, t;

		Vector3 E1, E2, T, P, Q;

		float det, inv_det;

		E1 = v1 - v0;
		E2 = v2 - v0;

		P = Vector3.Cross(rayDirection, E2);

		det = Vector3.Dot(E1, P);

		if ((det > -epsilon) && (det < epsilon))
			return -1f;

		inv_det = 1f / det;

		T = rayOrigin - v0;

		u = Vector3.Dot(T, P) * inv_det;

		if ((u < 0f) || (u > 1f))
			return -1f;

		Q = Vector3.Cross(T, E1);

		v = Vector3.Dot(rayDirection, Q) * inv_det;

		if ((v < 0f) || (u + v > 1f))
			return -1f;

		t = Vector3.Dot(E2, Q) * inv_det;

		intersection = (1f - u - v) * v0 + u * v1 + v * v2;

		return t;
	}
}
