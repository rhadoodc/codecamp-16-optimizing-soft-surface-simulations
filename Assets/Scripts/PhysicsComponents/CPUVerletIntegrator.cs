﻿using UnityEngine;

public class CPUVerletIntegrator : IVerletIntegrator
{
	public int HorizontalTiling
	{
		get; set;
	}

	public void Execute(int currentCacheIndex, ref Vector3[][] verticesCache, ref Vector3[] acceleration, bool[] pinned, float damping, float fixedDT, Vector3 gravity)
	{
		var oldVCache = verticesCache[currentCacheIndex];

		currentCacheIndex = (currentCacheIndex + 1) % 2;

		var currentVCache = verticesCache[currentCacheIndex];

		for (int i = 0; i < currentVCache.Length; i++)
		{
			if (pinned[i])
				continue;

			currentVCache[i] = oldVCache[i] + (oldVCache[i] - currentVCache[i]) * (1f - damping) + acceleration[i] * fixedDT;

			acceleration[i] = gravity; //reset influences outside of gravity
		}
	}

	public void Dispose()
	{

	}
}
