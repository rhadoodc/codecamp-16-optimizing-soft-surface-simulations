﻿using UnityEngine;

public class Constraint
{
	public int v1index;
	public int v2index;

	public readonly float restDistance;

	private static readonly Vector3 vZero = Vector3.zero;

	public enum EConstraintType
	{
		Stretch,
		Shear,
		Bend,
	}

	public EConstraintType type;

	public Constraint(int index1, int index2, ref Vector3[] vertices, EConstraintType type)
	{
		this.type = type;
		v1index = index1;
		v2index = index2;

		Vector3 vec = vertices[v1index] - vertices[v2index];
		restDistance = vec.magnitude;
	}

	public void SatisfyConstraint(ref Vector3[] vertices, ref bool[] pinned)
	{
		Vector3 diffVector = vertices[v1index] - vertices[v2index]; // vector from p1 to p2
		float currentDistance = diffVector.magnitude; // current distance between p1 and p2
		Vector3 correctionVector = diffVector * (currentDistance - restDistance); // The offset vector that could moves p1 into a distance of rest_distance to p2
		Vector3 correctionVectorHalf = correctionVector * 0.5f; // Lets make it half that length, so that we can move BOTH p1 and p2.

		vertices[v1index] = vertices[v1index] - (pinned[v1index] ? vZero : (pinned[v2index] ? correctionVector : correctionVectorHalf)); // correctionVectorHalf is pointing from p1 to p2, so the length should move p1 half the length needed to satisfy the constraint.
		vertices[v2index] = vertices[v2index] + (pinned[v2index] ? vZero : (pinned[v1index] ? correctionVector : correctionVectorHalf)); // we must move p2 the negative direction of correctionVectorHalf since it points from p2 to p1, and not p1 to p2.	
	}
}

