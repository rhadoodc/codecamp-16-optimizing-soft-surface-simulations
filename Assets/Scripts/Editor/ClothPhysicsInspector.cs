﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ClothPhysics), true, isFallback = true)]
public class ClothPhysicsInspector : TypedTargetInspector<ClothPhysics>
{
	private bool unfolded = false;

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		typedTarget.UseGPUIntegrator = EditorGUILayout.ToggleLeft("Use GPU Verlet", typedTarget.UseGPUIntegrator);

		typedTarget.verletIntegrationShader = (ComputeShader)EditorGUILayout.ObjectField("Verlet Integration Shader", typedTarget.verletIntegrationShader, typeof(ComputeShader), false);
		typedTarget.constraintShader = (ComputeShader)EditorGUILayout.ObjectField("Constraint Shader", typedTarget.constraintShader, typeof(ComputeShader), false);

		typedTarget.HorizontalSpacing = EditorGUILayout.FloatField("Horizontal Spacing", typedTarget.HorizontalSpacing);
		typedTarget.VerticalSpacing = EditorGUILayout.FloatField("Vertical Spacing", typedTarget.VerticalSpacing);
		typedTarget.HorizontalTiling = EditorGUILayout.IntField("Horizontal Tiling", typedTarget.HorizontalTiling);
		typedTarget.VerticalTiling = EditorGUILayout.IntField("Vertical Tiling", typedTarget.VerticalTiling);

		typedTarget.particleMass = EditorGUILayout.FloatField("Particle Mass", typedTarget.particleMass);

		typedTarget.constraintIterations = EditorGUILayout.IntField("Iterations", typedTarget.constraintIterations);
		typedTarget.damping = EditorGUILayout.Slider("Damping", typedTarget.damping, 1e-3f, 1f - 1e-3f);

		typedTarget.windDirection = EditorGUILayout.Vector3Field("Wind Direction", typedTarget.windDirection);
		typedTarget.windDirectionVariance = EditorGUILayout.Vector3Field("Wind Direction Variance", typedTarget.windDirectionVariance);

		typedTarget.windIntensity = EditorGUILayout.FloatField("Wind Intensity", typedTarget.windIntensity);
		typedTarget.windIntensityVariance = EditorGUILayout.FloatField("Wind Intensity Variance", typedTarget.windIntensityVariance);

		unfolded = EditorGUILayout.Foldout(unfolded, "Gizmo Prefs");

		if (unfolded)
		{
			typedTarget.showSurfaceNormals = EditorGUILayout.ToggleLeft("Show Surface Normals", typedTarget.showSurfaceNormals);
			typedTarget.showStretchConstraints = EditorGUILayout.ToggleLeft("Show Stretch Constraints", typedTarget.showStretchConstraints);
			typedTarget.showShearConstraints = EditorGUILayout.ToggleLeft("Show Shear Constraints", typedTarget.showShearConstraints);
			typedTarget.showBendConstraints = EditorGUILayout.ToggleLeft("Show Bend Constraints", typedTarget.showBendConstraints);
			typedTarget.showCollisions = EditorGUILayout.ToggleLeft("Show Collisions", typedTarget.showCollisions);
		}

		if (GUI.changed)
			EditorUtility.SetDirty(typedTarget);
	}
}
