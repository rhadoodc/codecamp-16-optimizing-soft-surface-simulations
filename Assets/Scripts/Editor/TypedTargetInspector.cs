﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class TypedTargetInspector<TObject> : Editor where TObject : MonoBehaviour
{
	protected TObject typedTarget = null;

	public void Awake()
	{
		typedTarget = (TObject)target;
	}

	public override void OnInspectorGUI()
	{
		Undo.RecordObject(typedTarget, string.Format("{0}[{1}] property changes", typedTarget.gameObject.name, typeof(TObject).Name));

		var oldEnabled = GUI.enabled;
		GUI.enabled = false;

		var targetScriptObject = MonoScript.FromMonoBehaviour(typedTarget);

		EditorGUILayout.ObjectField(targetScriptObject, typeof(TObject), false);

		GUI.enabled = oldEnabled;
	}
}
